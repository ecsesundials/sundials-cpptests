#include <iostream>
#include <dolfin.h>

using namespace dolfin;

#include <sundials/sundials_nvector.h>
#include "test_nvector.h"

extern "C"
{
  realtype get_element(N_Vector X, long int i)
  {
    auto nv = static_cast<dolfin::SUNDIALSNVector *>(X->content)->vec();
    std::vector<double> vals;
    nv->get_local(vals);
    return vals[i];
  }

  void set_element(N_Vector X, long int i, realtype val)
  {
    auto nv = static_cast<dolfin::SUNDIALSNVector *>(X->content)->vec();
    std::vector<double> vals;
    nv->get_local(vals);
    vals[i] = val;
    nv->set_local(vals);
    nv->apply("insert");
  }

  int check_ans(realtype ans, N_Vector X, long int local_length)
  {
    int fail = 0;
    auto nv = static_cast<dolfin::SUNDIALSNVector *>(X->content)->vec();

    std::vector<double> vals;
    nv->get_local(vals);
    nv->apply("insert");

    for (auto &v : vals)
      fail += (v != ans);

    return (fail != 0);
  }

  booleantype has_data(N_Vector X)
  {
    auto nv = static_cast<dolfin::SUNDIALSNVector *>(X->content)->vec();

    return (nv != NULL);
  }

}

int main(int argc, char* argv[])
{
  dolfin::SubSystemsManager::init_mpi();

  const int my_rank = dolfin::MPI::rank(MPI_COMM_WORLD);

  int global_size = 102;
  dolfin::SUNDIALSNVector nw(MPI_COMM_WORLD, global_size);
  dolfin::SUNDIALSNVector nx(MPI_COMM_WORLD, global_size);
  dolfin::SUNDIALSNVector ny(MPI_COMM_WORLD, global_size);
  dolfin::SUNDIALSNVector nz(MPI_COMM_WORLD, global_size);
  int local_size = nw.vec()->local_size();

  /* Create vectors */
  N_Vector W = nw.nvector();
  N_Vector X = nx.nvector();
  N_Vector Y = ny.nvector();
  N_Vector Z = nz.nvector();

  int fails = 0;

  // fails += Test_N_VSetArrayPointer(W, local_size, my_rank);
  // fails += Test_N_VGetArrayPointer(X, local_size, my_rank);
  fails += Test_N_VLinearSum(X, Y, Z, local_size, my_rank);
  fails += Test_N_VConst(X, local_size, my_rank);
  fails += Test_N_VProd(X, Y, Z, local_size, my_rank);
  fails += Test_N_VDiv(X, Y, Z, local_size, my_rank);
  fails += Test_N_VScale(X, Z, local_size, my_rank);
  fails += Test_N_VAbs(X, Z, local_size, my_rank);
  fails += Test_N_VInv(X, Z, local_size, my_rank);
  fails += Test_N_VAddConst(X, Z, local_size, my_rank);
  fails += Test_N_VDotProd(X, Y, local_size, global_size, my_rank);
  fails += Test_N_VMaxNorm(X, local_size, my_rank);
  fails += Test_N_VWrmsNorm(X, Y, local_size, my_rank);
  // fails += Test_N_VWrmsNormMask(X, Y, Z, local_size, local_size, my_rank);
  fails += Test_N_VMin(X, local_size, my_rank);
  // fails += Test_N_VWL2Norm(X, Y, local_size, local_size, my_rank);
  // fails += Test_N_VL1Norm(X, local_size, local_size, my_rank);
  fails += Test_N_VCompare(X, Z, local_size, my_rank);
  fails += Test_N_VInvTest(X, Z, local_size, my_rank);
  // fails += Test_N_VConstrMask(X, Y, Z, local_size, my_rank);
  //fails += Test_N_VMinQuotient(X, Y, local_size, my_rank);
  //fails += Test_N_VCloneVectorArray(5, X, local_size, my_rank);
  // fails += Test_N_VCloneEmptyVectorArray(5, X, my_rank);
  //fails += Test_N_VCloneEmpty(X, my_rank);
  fails += Test_N_VClone(X, local_size, my_rank);

  /* Print result */
  if (fails) {
    printf("FAIL: NVector module failed %i tests \n \n", fails);
  } else {
    printf("SUCCESS: NVector module passed all tests \n \n");
  }


  return 0;
}
