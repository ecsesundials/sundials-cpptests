#include <iostream>
#include <dolfin.h>

#include "Diffusion.h"

using namespace dolfin;

static int check_flag(void *flagvalue, const char *funcname, int opt)
{
  int *errflag;

  /* Check if SUNDIALS function returned NULL pointer - no memory allocated */

  if (opt == 0 && flagvalue == NULL) {
    fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed - returned NULL pointer\n\n",
            funcname);
    return(1); }

  /* Check if flag < 0 */

  else if (opt == 1) {
    errflag = (int *) flagvalue;
    if (*errflag < 0) {
      fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed with flag = %d\n\n",
              funcname, *errflag);
      return(1); }}

  /* Check if function returned NULL pointer - no memory allocated */

  else if (opt == 2 && flagvalue == NULL) {
    fprintf(stderr, "\nMEMORY_ERROR: %s() failed - returned NULL pointer\n\n",
            funcname);
    return(1); }

  return(0);
}
/*
static void PrintFinalStats(void *cvode_mem)
{
  int flag;
  long int nst, nfe, nsetups, netf, nni, ncfn, nje, nfeLS;

  flag = CVodeGetNumSteps(cvode_mem, &nst);
  check_flag(&flag, "CVodeGetNumSteps", 1);
  flag = CVodeGetNumRhsEvals(cvode_mem, &nfe);
  check_flag(&flag, "CVodeGetNumRhsEvals", 1);
  flag = CVodeGetNumLinSolvSetups(cvode_mem, &nsetups);
  check_flag(&flag, "CVodeGetNumLinSolvSetups", 1);
  flag = CVodeGetNumErrTestFails(cvode_mem, &netf);
  check_flag(&flag, "CVodeGetNumErrTestFails", 1);
  flag = CVodeGetNumNonlinSolvIters(cvode_mem, &nni);
  check_flag(&flag, "CVodeGetNumNonlinSolvIters", 1);
  flag = CVodeGetNumNonlinSolvConvFails(cvode_mem, &ncfn);
  check_flag(&flag, "CVodeGetNumNonlinSolvConvFails", 1);

  printf("\nFinal Statistics:\n");
  printf("nst = %-6ld nfe  = %-6ld nsetups = %-6ld \n",
	 nst, nfe, nsetups);
  printf("nni = %-6ld ncfn = %-6ld netf = %ld\n \n",
	 nni, ncfn, netf);

  return;
}
*/

// Class inheriting from dolfin::CVode and implementing the 'derivs' method
class MyCVode : public CVode
{
public:
  // FunctionSpace and solver should be set up before using

  std::shared_ptr<dolfin::Function> phi;
  std::shared_ptr<dolfin::FunctionSpace> V;
  std::shared_ptr<dolfin::LUSolver> lu;

  MyCVode(CVode::LMM cv_lmm = CVode::LMM::cv_bdf, CVode::ITER cv_iter = CVode::ITER::cv_newton) : CVode(cv_lmm, cv_iter)
  {

  }

  ~MyCVode()
  {

  }

  virtual void derivs(double t, std::shared_ptr<const GenericVector> u,
              std::shared_ptr<GenericVector> udot) override
  {
    Diffusion::LinearForm L(V);
    *(phi->vector()) = *u;

    L.phi = phi;
    auto b = std::make_shared<dolfin::Vector>();
    dolfin::assemble(*b, L);

    lu->solve(*udot, *b);

    //    std::cout << t << " " << "u.max() = " << u->max();
    //    std::cout << " udot.max() = " << udot->max() << "\n";
  }

  int jacobian(std::shared_ptr<const GenericVector> u,
               std::shared_ptr<GenericVector> Ju,
               double t, std::shared_ptr<const GenericVector> y,
               std::shared_ptr<const GenericVector> fy) override
  {
    return 0;
  }

};

class Source : public Expression
{
public:
  Source(double qin) : q(qin)
  {}

private:
  void eval(Array<double>& values, const Array<double>& x) const
  {
    double dx = x[0] - 0.5;
    double dy = x[1] - 0.5;
    values[0] = 10*exp(-q*(dx*dx + dy*dy));
  }

  double q;
};

int main(int argc, char* argv[])
{
//    dolfin::set_log_level(dolfin::LogLevel::DBG);

    dolfin::parameters["linear_algebra_backend"] = "PETSc";
  // Create CVode object with custom "derivs" implementation
  MyCVode cvode;

  auto mesh = std::make_shared<dolfin::UnitSquareMesh>(20, 20);
  cvode.V = std::make_shared<Diffusion::FunctionSpace>(mesh);
  cvode.phi = std::make_shared<Function>(cvode.V);
  auto phi = std::make_shared<Function>(cvode.V);


  // Set initial values
  double q = 20.0;
  Source source(q);
  phi->interpolate(source);

  // Assemble Mass Matrix
  Diffusion::BilinearForm a(cvode.V, cvode.V);
  auto A = std::make_shared<dolfin::Matrix>();
  dolfin::assemble(*A, a);

  std::cout << "Creating solver\n";
  cvode.lu = std::make_shared<LUSolver>(A);

  // Time
  double t = 1.0/(4*q);
  cvode.set_time(t);

  // Initialise CVode with a Vector and tolerances
  cvode.init(phi->vector(), 0.0, 1e-3, 2000);
  std::cout << t << "] phi=" << phi->vector()->max() << " ";
  std::cout << " t*phi=" << t*phi->vector()->max() << " ";
  std::cout << " sum=" << phi->vector()->sum() << "\n";

  //  TODO: when using NEWTON these will be needed
//  int flag;
//  if(cv_iter == CV_NEWTON)
//    flag = CVSpgmr(cvode_mem, PREC_LEFT, 0);
//  if(check_flag(&flag, "CVSpgmr", 1)) return(1);
//    flag = CVSpilsSetPreconditioner(cvode_mem, NULL, psolve);

  int n = 0;
  XDMFFile xdmf("a.xdmf");
  xdmf.write(*phi, t);

  const double dt = 0.001;
  Timer t0("ZZZ CVode solve");

  for (unsigned int i = 0; i < 25; ++i)
  {
    // Advance time
    t = cvode.step(dt);

    std::cout << t << "] phi=" << phi->vector()->max() << " ";
    std::cout << " t*phi=" << t*phi->vector()->max() << " ";
    std::cout << " sum=" << phi->vector()->sum() << "\n";
    std::cout << " i=" << i << "\n";
    dolfin_debug(std::to_string(i));
    xdmf.write(*phi, t);
  }

  t0.stop();
  list_timings(TimingClear::clear, {TimingType::wall});
  auto stats = cvode.statistics();
  std::cout << " Num steps=" << stats["Steps"] << "\n";
  std::cout << " RHS evals=" << stats["RHSEvals"] << "\n";
  std::cout << " LinSolverSetups=" << stats["LinSolvSetups"] << "\n";
  std::cout << " ErrTestFails=" << stats["ErrTestFails"] << "\n";
  std::cout << " NumNonlinSolvIter=" << stats["NumNonlinSolvIter"] << "\n";
  std::cout << " NumNonlinSolvConvFails=" << stats["NumNonlinSolvConvFails"] << "\n";
/*  std::cout << " Current order=" << stats["CurrentOrder"] << "\n";
  std::cout << " StabLimOrderReds=" << stats["StabLimOrderReds"] << "\n";
  std::cout << " ActualInitStep=" << stats["ActualInitStep"] << "\n";
  std::cout << " Last Step=" << stats["LastStep"] << "\n";
  std::cout << " Current Step=" << stats["CurrentStep"] << "\n";
  std::cout << " Current Time=" << stats["CurrentTime"] << "\n";
  std::cout << " TolScaleFactor=" << stats["TolScaleFactor"] << "\n";
  std::cout << " NumGEvals=" << stats["NumGEvals"] << "\n";
*/
  //  PrintFinalStats(cvode_mem);  // Print some final statistics
  // CVodeFree(&cvode_mem);  // Free the integrator memory

  return 0;
}
