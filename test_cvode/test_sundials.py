#!/usr/bin/python3

from dolfin import *

class MyCVode(CVode):

    # Overload the derivs method
    def derivs(self, t, u, udot):
        udot[:] = -u[:]
    def jacobian(self, u, udot, t, y, fy):
        return 0
    def JacobianSetup(self, t, Jv, y):
        print("Overloaded Jacobian setup")
    def psolve(self, t, u, udot, r, z, gamma, x, y):
        print("Overloaded psolve")
        return 0

def trace(frame, event, arg):
    print ("%s, %s:%d" % (event, frame.f_code.co_filename, frame.f_lineno))
    return trace

phi = Vector(MPI.comm_world,20)
phi[:] = 2.0
cv = MyCVode(CVode.LMM.CV_BDF,CVode.ITER.CV_NEWTON)
cv.set_time(0.0)
cv.init(phi, 1e-7, 1e-7)

nstep = 20
dt = 0.1
t = 0.0
for i in range(phi.get_local().size):
  t = cv.step(dt)
  print( phi.max(), (exp(-t) - phi.get_local()[i] ))
  if( (exp(-t) - phi.get_local()[i]) >= 1e-7 ):
    print("Solution failed")

print(phi.get_local().size)
stats = cv.statistics()
print(stats['Steps'])
print(stats['RHSEvals'])
  #print(stats['GEvals'])

