#!/usr/bin/python3

import dolfin as df
#import df.CVode as cvode
df.parameters['linear_algebra_backend'] = 'PETSc'

import ufl.log as ufl_log
import numpy as np
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt

# Suppress nonsense UFL messages
ufl_log.set_level("CRITICAL")

# Material parameters
Ms = 8.6e5  # saturation magnetisation (A/m)
alpha = 0.1  # Gilbert damping
gamma = 2.211e5  # gyromagnetic ratio
A = 1e-11 # exchange constant (J/m)

# External magentic field.
B = 0.1  # (T)
mu0 = 4 * np.pi * 1e-7  # vacuum permeability

# Zeeman field
H = Ms / 2 * df.Constant((0,0,1))
# meaningful time period is of order of nano seconds
dt = 1e-12

# mesh parameters
d = 10e-9
thickness = 10e-9
nx = ny = 5
nz = 2

# create mesh
p1 = df.Point(0, 0, 0)
p2 = df.Point(d, d, thickness)
mesh = df.BoxMesh(p1, p2, nx, ny, nz)

# define function space for magnetization
V = df.VectorFunctionSpace(mesh, "CG", 1, dim=3)
v = df.TestFunction(V)
u = df.TrialFunction(V)

# define initial M and normalise
m = df.Function(V)

def effective_field(m, volume=None):
    w_Zeeman = - mu0 * Ms * df.dot(m, H)
    w_exchange = A  * df.inner(df.grad(m), df.grad(m))
    w = w_Zeeman + w_exchange
    return -1/mu0/Ms * df.derivative(w*df.dx, m)

# Effective field
Heff_form = effective_field(m)

# Preassemble projection Matrix
Amat = df.assemble(df.dot(u, v)*df.dx)

LU = df.LUSolver()
LU.set_operator(Amat)
Heff = df.Function(V)

def compute_dmdt(m):
    """Convenience function that does all in one go"""

    # Assemble RHS
    b = df.assemble(Heff_form)

    # Project onto Heff
    LU.solve(Heff.vector(), b)

    LLG = -gamma/(1+alpha*alpha)*df.cross(m, Heff) - alpha*gamma/(1+alpha*alpha)*df.cross(m, df.cross(m, Heff))

    result = df.assemble(df.dot(LLG, v)*df.dP)
    return result[:]

# function for integration of system of ODEs

def rhs_micromagnetic(m_vector_array, t, counter=[0]):
    assert isinstance(m_vector_array, np.ndarray)
    m.vector()[:] = m_vector_array[:]

    dmdt = compute_dmdt(m)
    return dmdt

class Integrator(df.CVode):
    def derivs(self, t, u, udot):
        udot[:] = rhs_micromagnetic(u[:], t)
    def jacobian(self, u, udot, t, y, fy):
        return 0
    def psolve(self, t, u, udot, r, z, gamma, x, y):
        return 0

n = 100
m.interpolate(df.Constant((1, 0, 0)))
ts = np.linspace(0, 5e-11, n)

use_cvode = True
if (use_cvode):
    cvode = Integrator(df.CVode.LMM.CV_BDF,df.CVode.ITER.CV_NEWTON)
    cvode.init(m.vector(), 1e-3, 1e-3)

    ms = np.zeros((n, 3))
    ms[0] = m.vector()[0:3]
    for i in range(1, n):
        dt = ts[i] - ts[i - 1]
        t = cvode.step(dt)
        print(t, m.vector()[0:3])
        ms[i] = m.vector()[0:3]
        plt.plot(ts, ms[:,0:3], '-x')
        filename = "/tmp/test"+str(i)+".png"
        plt.savefig(filename)
else:
    from scipy.integrate import odeint
    ms = odeint(rhs_micromagnetic, y0=m.vector().array(), t=ts)

#plt.show()
