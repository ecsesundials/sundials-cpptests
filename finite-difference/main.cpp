// Test code to run the diffusion equation in 1D
// with DOLFIN as the driver

#include <dolfin.h>

#include <math.h>
#include <stdio.h>

#include <cvode/cvode.h>
#include <cvode/cvode_impl.h>
#include <cvode/cvode_spils.h>
#include <sunlinsol/sunlinsol_spgmr.h>
#include <sundials/sundials_dense.h>
#include <sundials/sundials_types.h>
#include <sundials/sundials_iterative.h>

FILE *fp;

int print_u(N_Vector u)
{
  int n = 100;

  std::vector<double> udata(n);
  auto v = static_cast<dolfin::SUNDIALSNVector *>(u->content)->vec();
  v->get_local(udata);

  for (unsigned int i = 0; i < n; ++i)
    fprintf(fp, "%f\n", udata[i]);
  fprintf(fp, "\n\n");
}


int f(realtype t, N_Vector u, N_Vector udot, void *user_data)
{
  int n = 100;

  auto v = static_cast<dolfin::SUNDIALSNVector *>(u->content)->vec();
  auto dv = static_cast<dolfin::SUNDIALSNVector *>(udot->content)->vec();

  std::vector<double> dudata(n);
  std::vector<double> udata(n);
  v->get_local(udata);

  // Finite difference laplacian operator in 1D
  for (unsigned int i = 1; i < (n - 1); ++i)
    dudata[i] = udata[i-1] - 2*udata[i] + udata[i+1];

  print_u(u);

  dv->set_local(dudata);
  dv->apply("insert");

  return 0;
}

int init_u(N_Vector u)
{
  int n = 100;

  std::vector<double> udata(n);

  for (unsigned int i = 0; i < n; ++i)
  {
    double r = ((double)(i)-(n/2))/(double)n;
    udata[i] = exp(-50*r*r);
  }

  auto v = static_cast<dolfin::SUNDIALSNVector *>(u->content)->vec();
  v->set_local(udata);
  v->apply("insert");
}

// Jacobian times vector - no-op
static int jtv(N_Vector v, N_Vector Jv, realtype t,
               N_Vector u, N_Vector fu,
               void *user_data, N_Vector tmp)
{
  return 0;
}

// Preconditioner solver - no-op
static int PSolve(realtype tn, N_Vector u, N_Vector fu, N_Vector r, N_Vector z,
                  realtype gamma, realtype delta, int lr, void *user_data)
{
  return 0;
}



int solve_cvode()
{
  dolfin::parameters["linear_algebra_backend"] = "Eigen";

  //  dolfin::set_log_level(dolfin::LogLevel::DBG);

  fp = fopen("data-dolfin.txt", "w");

  int n = 100;
  double t = 0.0;
  void *cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON);

  auto u_dolfin = dolfin::SUNDIALSNVector(MPI_COMM_WORLD, n);
  N_Vector u = u_dolfin.nvector();

  SUNLinearSolver LS = SUNSPGMR(u, PREC_LEFT, 0);

  init_u(u);

  int flag = CVodeInit(cvode_mem, f, t, u);

  double atol = 1e-6;
  double rtol = 1e-6;
  flag = CVodeSStolerances(cvode_mem, rtol, atol);
  printf("# %d\n", flag);

  flag = CVSpilsSetLinearSolver(cvode_mem, LS);
  printf("# %d\n", flag);

  flag = CVSpilsSetJacTimes(cvode_mem, NULL, jtv);
  printf("# %d\n", flag);

  flag = CVSpilsSetPreconditioner(cvode_mem, NULL, PSolve);
  printf("# %d\n", flag);

  double dt = 1.0;

  for (unsigned int i = 0; i < 50; ++i)
  {
    print_u(u);
    double tout = t + dt;
    CVode(cvode_mem, tout, u, &t, CV_NORMAL);
  }

  CVodeFree(&cvode_mem);
  return 0;
}

class MyCVode: public dolfin::CVode
{
public:
  MyCVode(dolfin::CVode::LMM cv_lmm, dolfin::CVode::ITER cv_iter) : CVode(cv_lmm, cv_iter)
  {
  }

  virtual void derivs(double t, std::shared_ptr<const dolfin::GenericVector> u,
                      std::shared_ptr<dolfin::GenericVector> udot)
  {

    const std::size_t n = u->size();
    std::vector<double> dudata(n);
    std::vector<double> udata(n);
    u->get_local(udata);

    // Finite difference laplacian operator in 1D
    for (unsigned int i = 1; i < (n - 1); ++i)
      dudata[i] = udata[i-1] - 2*udata[i] + udata[i+1];

    udot->set_local(dudata);
    udot->apply("insert");
  }

  virtual int jacobian(std::shared_ptr<const dolfin::GenericVector> v,
                       std::shared_ptr<dolfin::GenericVector> Jv,
                       double t, std::shared_ptr<const dolfin::GenericVector> y,
                       std::shared_ptr<const dolfin::GenericVector> fy)
  {
    return 0;
  }

  virtual int psolve(double tn, std::shared_ptr<const dolfin::GenericVector>y,
                     std::shared_ptr<const dolfin::GenericVector> fy,
                     std::shared_ptr<const dolfin::GenericVector> r,
                     std::shared_ptr<dolfin::GenericVector> z,
                     double gamma, double delta, int lr)
  {
    return 0;
  }

};

int solve_dolfin()
{

  MyCVode my_cvode(dolfin::CVode::LMM::cv_bdf, dolfin::CVode::ITER::cv_newton);

  int n = 100;

  auto u = std::make_shared<dolfin::Vector>(MPI_COMM_WORLD, n);
  std::vector<double> udata(n);
  for (unsigned int i = 0; i < n; ++i)
  {
    double r = ((double)(i)-(n/2))/(double)n;
    udata[i] = exp(-50*r*r);
  }

  u->set_local(udata);
  u->apply("insert");

  my_cvode.init(u, 1e-6, 1e-6, 2000);

  double dt = 1.0;

  fp = fopen("data-dolfin2.txt", "w");

  for (unsigned int i = 0; i < 50; ++i)
  {
    // Advance time
    double t = my_cvode.step(dt);

    std::cout << t << "] umax =" << u->max() << " ";
    std::cout << " t*umax =" << t*u->max() << " ";
    std::cout << " sum =" << u->sum() << "\n";

    u->get_local(udata);
    for (unsigned int i = 0; i < n; ++i)
      fprintf(fp, "%f\n", udata[i]);
    fprintf(fp, "\n\n");

  }

  fclose(fp);

}


int main(int argc, char *argv[])
{

  solve_dolfin();

  // solve_cvode();

  return 0;
}
